<?php
namespace classes;
class Warehouse
{
    private $products;

    public function __construct()
    {
        $this->products = array();
    }

    public function addProduct($name, $unit, $unit_price, $quantity, $last_stock_date, $categories)
    {
        $product = array(
            'name' => $name,
            'unit' => $unit,
            'unit_price' => $unit_price,
            'quantity' => $quantity,
            'last_stock_date' => $last_stock_date,
            'categories' => $categories
        );
        $this->products[] = $product;
    }

    public function getProducts()
    {
        return $this->products;
    }

    public function getProductByName($name)
    {
        foreach ($this->products as $product) {
            if ($product['name'] === $name) {
                return $product;
            }
        }
        return null;
    }

    public function updateProductQuantity($name, $quantity)
    {
        foreach ($this->products as $key => $product) {
            if ($product['name'] === $name) {
                $this->products[$key]['quantity'] = $quantity;
                return true;
            }
        }
        return false;
    }

    public function removeProductByName($name)
    {
        foreach ($this->products as $key => $product) {
            if ($product['name'] === $name) {
                unset($this->products[$key]);
                return true;
            }
        }
        return false;
    }

}


