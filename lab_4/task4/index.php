<?php
include_once ('FileSystemImageLoadingStrategy.php');
include_once ('NetworkImageLoadingStrategy.php');
include_once ('Image.php');

use task4\FileSystemImageLoadingStrategy;
use task4\NetworkImageLoadingStrategy;
use task4\Image;


$fileSystemStrategy = new FileSystemImageLoadingStrategy();
$image = new Image($fileSystemStrategy);
$imageData=$image->loadImage("C://wamp64//domains//konstr_prog//lab_4//task4//download.jpg");
echo '<img src="data:image/jpeg;base64,' . base64_encode($imageData) . '"/>';

 //або можемо використовувати стратегію завантаження з мережі
$networkStrategy = new NetworkImageLoadingStrategy();
$image = new Image($networkStrategy);
$imageData1=$image->loadImage("http://staffmobility.eu/sites/default/files/isewtweetbg.jpg");
echo '<img  width=200px; src="data:image/jpeg;base64,' . base64_encode($imageData1) . '"/>';
