<?php

// Включення класів
include_once('LightNode.php');
include_once('LightTextNode.php');
include_once('LightElementNode.php');
include_once('Observer.php');

use task3\LightTextNode;
use task3\LightElementNode;
use task3\Observer;

// Клас для тестування
class ExampleObserver implements Observer {
    public function update(string $event, $data) {
        echo "Received update for event '{$event}' with data: {$data}\n";
    }
}

// Створення об'єктів
$textNode = new LightTextNode("This is a text node");
$elementNode = new LightElementNode("div", "block", "closing");

// Прикріплення спостерігачів
$observer1 = new ExampleObserver();
$observer2 = new ExampleObserver();

$textNode->attach($observer1, 'text_changed');
$elementNode->attach($observer2, 'element_clicked');

// Створення кнопки
$button = new LightElementNode("button", "inline", "closing");
$buttonText = new LightTextNode("Натисніть кнопку");

// Додавання тексту до кнопки
$button->addChild($buttonText);

// Прикріплення події до кнопки
$button->attachEvent('click', function ($data) use ($observer1) {
    $observer1->update('button_clicked', $data);
});

// Додавання кнопки до елементу
$elementNode->addChild($button);

// Виведення HTML
echo $elementNode->getOuterHTML();
?>
