<?php

namespace task3;
include_once ('Subject.php');
use task3\Subject;
abstract class LightNode implements Subject
{
    protected $observers = [];

    public function attach(Observer $observer, string $event) {
        $this->observers[$event][] = $observer;
    }

    public function detach(Observer $observer, string $event) {
        if(isset($this->observers[$event])) {
            $key = array_search($observer, $this->observers[$event], true);
            if($key !== false) {
                unset($this->observers[$event][$key]);
            }
        }
    }

    public function notify(string $event, $data) {
        if(isset($this->observers[$event])) {
            foreach ($this->observers[$event] as $observer) {
                $observer->update($event, $data);
            }
        }
    }

    abstract public function getOuterHTML();
    abstract public function getInnerHTML();
}
