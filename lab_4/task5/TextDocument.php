<?php

namespace task5;
class TextDocument {
    private $content;

    public function __construct($content) {
        $this->content = $content;
    }

    public function getContent() {
        return $this->content;
    }

    public function setContent($content) {
        $this->content = $content;
    }

    public function createMemento() {
        return new TextDocumentMemento($this->content);
    }

    public function restoreFromMemento(TextDocumentMemento $memento) {
        $this->content = $memento->getContent();
    }
}