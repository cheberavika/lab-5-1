<?php
include_once('Logger.php');
include_once('FileWriter.php');

class FileLoggerAdapter
{
    private $logger;

    public function __construct($fileWriter)
    {
        $this->logger = new Logger();
        $this->fileWriter = $fileWriter;
    }

    public function Log()
    {
        $this->logger->echoColored("Green message", "green");
        $this->fileWriter->WriteLine("Green message");
    }

    public function Error()
    {
        $this->logger->echoColored("Red message", "red");
        $this->fileWriter->WriteLine("Red message");
    }

    public function Warn()
    {
        $this->logger->echoColored("Yellow message", "yellow");
        $this->fileWriter->WriteLine("Yellow message");
    }
}
