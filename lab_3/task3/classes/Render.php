<?php

namespace task3\classes;
interface Render
{
    public function renderShape();
}