<?php

namespace task2;

interface Hero
{
    public function getDescription();
}