<?php

namespace task2;

class WeaponDecorator extends EquipmentDecorator
{
    public function getDescription()
    {
        return parent::getDescription() . ", weapon";
    }
}