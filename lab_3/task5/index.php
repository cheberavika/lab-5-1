<?php
include_once('LightNode.php');
include_once('LightElementNode.php');
include_once('LightTextNode.php');

use task5\LightElementNode;
use task5\LightNode;
use task5\LightTextNode;

$style = new LightElementNode('style', 'block', 'closing');
$css = "
.my-table {
    width: 50%;
    border-collapse: collapse;
    margin: 20px;
}

 th,  td {
    border: 1px solid black;
    padding: 8px;
    text-align: left;
}

thead {
    background-color: lightpink;
}

";
$style->addChild(new LightTextNode($css));
$head = new LightElementNode('head', 'block', 'closing');
$head->addChild($style);

$table = new LightElementNode('table', 'block', 'closing');
$table->addClass('my-table');

// Додаємо заголовок таблиці
$thead = new LightElementNode('thead', 'block', 'closing');
$trHead = new LightElementNode('tr', 'block', 'closing');
$th1 = new LightElementNode('th', 'block', 'closing');
$th1->addChild(new LightTextNode('Header 1'));
$trHead->addChild($th1);

$th2 = new LightElementNode('th', 'block', 'closing');
$th2->addChild(new LightTextNode('Header 2'));
$trHead->addChild($th2);

$thead->addChild($trHead);
$table->addChild($thead);

// Додаємо тіло таблиці
$tbody = new LightElementNode('tbody', 'block', 'closing');
$tr1 = new LightElementNode('tr', 'block', 'closing');
$td1 = new LightElementNode('td', 'block', 'closing');
$td1->addChild(new LightTextNode('Row 1, Cell 1'));
$tr1->addChild($td1);

$td2 = new LightElementNode('td', 'block', 'closing');
$td2->addChild(new LightTextNode('Row 1, Cell 2'));
$tr1->addChild($td2);

$tbody->addChild($tr1);

$tr2 = new LightElementNode('tr', 'block', 'closing');
$td3 = new LightElementNode('td', 'block', 'closing');
$td3->addChild(new LightTextNode('Row 2, Cell 1'));
$tr2->addChild($td3);

$td4 = new LightElementNode('td', 'block', 'closing');
$td4->addChild(new LightTextNode('Row 2, Cell 2'));
$tr2->addChild($td4);

$tbody->addChild($tr2);

$table->addChild($tbody);

// Виведення HTML таблиці
echo $head->getOuterHTML();
echo $table->getOuterHTML();