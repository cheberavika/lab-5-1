<?php

namespace task4;
include_once('SmartTextReader.php');

class SmartTextReaderLocker extends SmartTextReader
{
    private $smartTextReader;
    private $allowedPattern;

    public function __construct(SmartTextReader $smartTextReader, $allowedPattern) {
        $this->smartTextReader = $smartTextReader;
        $this->allowedPattern = $allowedPattern;
    }

    public function readTextFile($filename) {
        if (!preg_match($this->allowedPattern, $filename)) {
            echo "Access denied!\n";
            return null;
        } else {
            return $this->smartTextReader->readTextFile($filename);
        }
    }

}