<?php
namespace classes;
class DomesticSubscription extends Subscription
{
    public function getInformation()
    {
        return "Domestic Subscription: Monthly Fee - {$this->monthlyFee}, Minimum Period - {$this->minPeriod}, 
        Channels - " . implode(', ', $this->listOfChannels) . ", Features - " . implode(', ', $this->features);

    }
}