<?php
namespace classes;
abstract class SubscriptionFactory {
    abstract public function createSubscription();
}