<?php

require_once ('Authenticator.php');

$auth1 = Authenticator::getInstance();
$auth2 = Authenticator::getInstance();

// Перевірка чи $auth1 та $auth2 вказують на один і той же об'єкт
echo var_dump($auth1 === $auth2) . '<br>'; // повинно вивести bool(true)

// Приклад використання
echo $auth1->authenticate("user1", "password1") . '<br>';
echo $auth2->authenticate("user2", "password2") . '<br>';

?>
